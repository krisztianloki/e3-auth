## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS


SCRIPTS += $(wildcard ../template/*.acf)
SCRIPTS +=$(wildcard ../iocsh/*.iocsh)


.PHONY: db
db:
.PHONY: vlibs
vlibs:
